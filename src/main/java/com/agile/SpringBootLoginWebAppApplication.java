package com.agile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.agile","com.agile.controller","com.agile.model","com.agile.service"})
public class SpringBootLoginWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootLoginWebAppApplication.class, args);
	}

}

