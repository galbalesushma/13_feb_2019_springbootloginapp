package com.agile.service;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.ServerEndpoint;

import org.springframework.stereotype.Service;

import com.agile.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	List<Employee> list = new ArrayList<>();

	public List<Employee> create() {
		list.add(new Employee("Charles", "Charles"));
		list.add(new Employee("Babbage", "Babbage"));
		list.add(new Employee("Sai", "Sai"));
		return list;

	}

	@Override
	public boolean verify(String empName, String empPass) {
		if (empName.equals("agile") && empPass.equals("agile"))
			return true;
		else

			return false;
	}

	@Override
	public boolean validate(String empName, String empPass, List<Employee> list) {

		for (Employee e : list) {
			if (empName.equals(e.getEmpName()) && empName.equals(e.getEmpPass())) {
				return true;

			}
		}
		return false;

	}

}
