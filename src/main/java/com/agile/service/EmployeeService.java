package com.agile.service;

import java.util.List;

import com.agile.model.Employee;

public interface EmployeeService {
	public List<Employee> create();

	public boolean verify(String empName, String empPass);
	
	
	public boolean validate(String empName, String empPass,List<Employee> list);
}
