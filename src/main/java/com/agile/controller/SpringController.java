package com.agile.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.agile.model.Employee;
import com.agile.service.EmployeeService;

@Controller
public class SpringController {

	@Autowired
	Employee employee;

	@Autowired
	EmployeeService employeeService;

	@RequestMapping(value = "/")
	public String welcome(Model model) {
		model.addAttribute("Employee", employee);
		return "home";
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.POST)
	public String welcomeUser(@RequestParam("empName") String name, @RequestParam("empPass") String pass, Model model) {
		System.out.println(name);
		List<Employee> list = employeeService.create();
		System.out.println(list.toString());
		if(name.isEmpty() || pass.isEmpty())
		{
			return "error";
		}
		else
		{
		if (employeeService.validate(name, pass, list)) {
            model.addAttribute("name",name);
            return "welcome";
		}
		else
		return "error";
		}
		
	}
}