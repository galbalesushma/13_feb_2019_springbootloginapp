package com.agile.model;

import org.springframework.stereotype.Component;

@Component
public class Employee {

	private String empName;
	private String empPass;

	public String getEmpPass() {
		return empPass;
	}

	public Employee() {

	}

	@Override
	public String toString() {
		return "Employee [empName=" + empName +  "]";
	}

	public Employee(String empName, String empPass) {
		super();
		this.empName = empName;
		this.empPass = empPass;
	}

	public void setEmpPass(String empPass) {
		this.empPass = empPass;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

}